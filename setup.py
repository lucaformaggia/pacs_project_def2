#!/usr/bin/env python
from setuptools import setup, find_packages
from setuptools.extension import Extension
import numpy as np

import os

if os.path.exists('MANIFEST'):
    os.remove('MANIFEST')

setup(name="fenics_tumor",
      version="0.0.1",
      zip_safe=False,
      #      packages = find_packages('fenics_ironman'),  # include all packages under src
#      package_dir = {'':'fenics_ironman'},   # tell distutils packages are under src
    packages=["fenics_tumor"],
     include_package_data=True,
      description="interface model for tumor invasion in Fenics",
      author="Daniele Boaretti",
      author_email="daniele.boaretti@mail.polimi.com",
      license="GPL 1-clause",
      use_2to3=True,
      classifiers=['Intended Audience :: Science/Research',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved',
                   'Programming Language :: Python',
                   'Topic :: Scientific/Engineering',
                   'Operating System :: Microsoft :: Windows',
                   'Operating System :: POSIX',
                   'Operating System :: Unix',
                   'Operating System :: MacOS',
                   'Programming Language :: Python :: 2',
                   'Programming Language :: Python :: 2.6',
                   'Programming Language :: Python :: 2.7',
                   'Programming Language :: Python :: 3',
                   'Programming Language :: Python :: 3.3',
                   ],
      include_dirs=[np.get_include()],
      url='https://bitbucket.org/daniboa/pacs_project_def2/src'
      )

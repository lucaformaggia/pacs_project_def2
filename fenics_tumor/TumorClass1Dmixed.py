"""
Unit test for TumorClass
"""
__all__ = ['TumorClass1Dmixed']
from dolfin import *
from SolverClass import *
from numpy import size

class TumorClass1Dmixed():
    def  __init__(self,data,BCvec):
        """
        This Class creates the problem for the 1D case with mixed approach.

        Args:
            data (dict): Collection of all physical data
            BCvec (list(Constant)): Collection of all boundary conditions

        Raises:
            AttributeError: The ``Raises`` section is a list of all exceptions
                that are relevant to the interface.
            ValueError: If a parameter is the data dictionary is not a constant,
                it raise a warning.

        """

        #: We set the parameters
        try:
            self.dict = {
            'mu': data['mu'],
            'nu': data['nu'],
            'Pm': data['Pm'],
            'gamma': data['gamma'],
            'Lx': data['Lx'],
            'Rx': data['Rx'],
            'nx': data['nx'],
            'dt': data['dt'],
            'T': data['T'],
            'mesh': data['mesh'],
            'mn_init' : data['mn_init'],
            'filehandler': data['filehandler'],
            'system': data['system']
            }


        except ValueError:
            if not isinstance(mu,dolfin.Constant):
                warning.warn("mu should be Constant!!")
            if not isinstance(nu,dolfin.Constant):
                warning.warn ("nu should be Constant!!")
            if not isinstance(Pm,dolfin.Constant):
                warning.warn ("Pm should be Constant!!")
            if not isinstance(gamma,dolfin.Constant):
                warning.warn ("gamma should be Constant!!")
            if not isinstance(Lx,dolfin.Constant):
                warning.warn ("Lx should be Constant!!")
            if not isinstance(nx,dolfin.Constant):
                warning.warn ("nx should be Constant!!")
            if not isinstance(Rx,dolfin.Constant):
                warning.warn ("Rx should be Constant!!")
            if not isinstance(dt,dolfin.Constant):
                warning.warn ("dt should be Constant!!")
            if not isinstance(T,dolfin.Constant):
                warning.warn ("T should be Constant!!")

        #: Declaration of all variables
        self.bcs = []
        self.F_form = None
        self.J_form = None
        self.mf = None
        self.FEspaces = None
        self.u_h = None
        self.mn0_ = None
        self.filehandler = None


        #: Construction of the problem

        self.AssembleProblem()
        self.reset_sparsity=True
        self.bcs = self.AssembleBC(BCvec)



    def spaceFem(self):
        """

        Here I define the Functional Spaces for the variational problem

        Return:
            It returns the FE spaces.

        """
        #: If I have the problem only for m:
        #: Case for only m

        if (self.dict['system'] =='m'):
            if (self.dict['dt']==0 and self.dict['Pm']!=0):
                M = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
                W = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 2)
                MWmix = MixedElement([M,W])
                M_fe = FunctionSpace(self.dict['mesh'], M)
                W_fe = FunctionSpace(self.dict['mesh'], W)
                MW = FunctionSpace(self.dict['mesh'], MWmix)
                self.FEspaces = {
                'M': M_fe,
                'W': W_fe,
                'MW': MW,
                }
            else:
                M = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
                W = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
                MWmix = MixedElement([M,W])
                M_fe = FunctionSpace(self.dict['mesh'], M)
                W_fe = FunctionSpace(self.dict['mesh'], W)
                MW = FunctionSpace(self.dict['mesh'], MWmix)
                self.FEspaces = {
                'M': M_fe,
                'W': W_fe,
                'MW': MW,
                }
        #: Otherwise I have the problem for m and n:
        else:
            M = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
            N = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
            W = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
            MNWmix = MixedElement([M,N,W])
            M_fe = FunctionSpace(self.dict['mesh'], M)
            N_fe = FunctionSpace(self.dict['mesh'], N)
            W_fe = FunctionSpace(self.dict['mesh'], W)
            MNW = FunctionSpace(self.dict['mesh'], MNWmix)
            self.FEspaces = {
            'M': M_fe,
            'N': N_fe,
            'W': W_fe,
            'MNW': MNW,
            }

    def NonStationary1D_ad_re_m_n(self):
        """
        Assemble the full nonstationary variational form for m and n.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        MNW = self.FEspaces['MNW']


        #Solution
        m = Function(MNW)
        (m_,n_,w_) = split(m)
        self.mn0_ = Function(MNW)
        (m0_,n0_, w0_) = split(self.mn0_)


        # Test function
        dm_ = TestFunction(MNW)
        (dm,dn,dw) = split(dm_)

        # incremental solution
        ddm = TrialFunction(MNW)


      	#: G expr
      	G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))


        dt = self.dict['dt']
        mu = self.dict['mu']
        nu= self.dict['nu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})

        F0 = -dt*mu*(inner(grad(m_)[0],w_) + m_*grad(w_)[0])*dm*dxCustom\
                -dt*G(m_,n_,Pm,gamma)*m_*dm*dxCustom +( m_-m0_)*dm*dxCustom

        F1 = -dt*nu*(inner(grad(n_)[0],w_) + n_*grad(w_)[0])*dn*dxCustom \
                +( n_-n0_)*dn*dxCustom

        F2 = w_* dw*dxCustom +\
            K(gamma)*inner(grad(dw)[0],(m_+n_)**gamma)*dxCustom

        F = F0 + F1 + F2

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], MNW)

        F_h = replace(F, {m: self.u_h})


        self.F_form = F_h


        J = derivative(F,m,ddm)
        J_h = replace(J, {m: self.u_h})

        self.J_form = J_h

    def NonStationary1D_ad_re_m(self):
        """Assemble the nonstationary variational form with adv. and reac.
        for m.



        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

      	#: G expr
      	G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))

        self.spaceFem()

        #: Now I define the variables for the hole problem
        MW = self.FEspaces['MW']

        #Solution
        m = Function(MW)
        (m_,w_) = split(m)
        self.mn0_ = Function(MW)
        (m0_,w0_) = split(self.mn0_)


        # Test function
        dm = TestFunction(MW)
        (dm,dw) = split(dm)

        # incremental solution
        ddm = TrialFunction(MW)



        dt = self.dict['dt']
        mu = self.dict['mu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})

        F0 = -dt*mu*(inner(grad(m_)[0],w_) + m_*grad(w_)[0])*dm*dxCustom\
                -dt*G(m_,0.,Pm,gamma)*m_*dm*dxCustom +( m_-m0_)*dm*dxCustom

        F1 = w_* dw*dxCustom + K(gamma)*inner(grad(dw)[0],m_**gamma)*dxCustom

        F = F0 + F1

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], MW)

        F_h = replace(F, {m: self.u_h})


        self.F_form = F_h


        J=derivative(F,m,ddm)
        J_h = replace(J, {m: self.u_h})

        self.J_form = J_h

    def NonStationary1D_ad_m(self):
        """
        Assemble the nonstationary variational form with advection for m.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        MW = self.FEspaces['MW']

        #Solution
        m = Function(MW)
        (m_,w_) = split(m)
        self.mn0_ = Function(MW)
        (m0_,w0_) = split(self.mn0_)


        # Test function
        dm = TestFunction(MW)
        (dm,dw) = split(dm)

        # incremental solution
        ddm = TrialFunction(MW)


        dt = self.dict['dt']
        mu = self.dict['mu']
        gamma = self.dict['gamma']

        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})

        F0 = -dt*mu*(inner(grad(m_)[0],w_) + m_*grad(w_)[0])*dm*dxCustom\
            +( m_-m0_)*dm*dxCustom


        F1 = w_* dw*dxCustom + K(gamma)*inner(grad(dw)[0],m_**gamma)*dxCustom

        F = F0 + F1

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], MW)

        F_h = replace(F, {m: self.u_h})


        self.F_form = F_h


        J=derivative(F,m,ddm)
        J_h = replace(J, {m: self.u_h})

        self.J_form = J_h

    def Stationary1D_ad_re_m(self):
        """
        Assemble the stationary variational form with adv. and reac. for m.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        MW = self.FEspaces['MW']


        #Solution
        m = Function(MW)
        (m_,w_) = split(m)

        # Test function
        dm_ = TestFunction(MW)
        (dm,dw) = split(dm_)

        # incremental solution
        ddm = TrialFunction(MW)

        self.u_h = interpolate(self.dict['mn_init'], MW)

      	#: G expr
      	G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))



        mu = self.dict['mu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})

        F0 = -mu*(inner(grad(m_)[0],w_) + m_*grad(w_)[0])*dm*dxCustom\
                -G(m_,0.,Pm,gamma)*m_*dm*dxCustom


        F1 = w_* dw*dxCustom + K(gamma)*inner(grad(dw)[0],m_**gamma)*dxCustom

        F = F0 + F1

        F_h = replace(F, {m: self.u_h})

        self.F_form = F_h

        J = derivative(F,m ,ddm)

        J_h = replace(J, {m: self.u_h})

        self.J_form = J_h


    def Stationary1D_ad_m(self):
        """
        Assemble the stationary variational form  with advection for m.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        MW = self.FEspaces['MW']

        #Solution
        self.u_h = Function(MW)
        (m_,w_) = split(self.u_h)


        # Test function
        dm = TestFunction(MW)
        (dm,dw) = split(dm)

        # incremental solution
        ddm = TrialFunction(MW)


        mu = self.dict['mu']
        gamma = self.dict['gamma']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})

        F0 = -mu*(inner(grad(m_)[0],w_) + m_*grad(w_)[0])*dm*dxCustom\

        F1 = w_* dw*dxCustom + K(gamma)*inner(grad(dw)[0],w_)*dxCustom

        F = F0 + F1


        self.F_form = F


        J=derivative(F,self.u_h,ddm)

        self.J_form = J


    def AssembleProblem(self):
        """Assemble the problem with the variational form.

        Note:
            The variational form may vary from one file to other, since the kind
            of problem to solve may change due to the test.


        """
        #: Case for only m
        if (self.dict['system']=='m'):
            #: Stationary case
            if (self.dict['dt']==0):
                #: Only advection
                if (self.dict['Pm']==0):
                    self.Stationary1D_ad_m()
                #: With advection and reaction
                else:
                    self.Stationary1D_ad_re_m()
            #: Non-stationary only for m:
            else:
                #: Only advection
                if (self.dict['Pm']==0):
                    self.NonStationary1D_ad_m()
                else:
                    #: With advection and reaction
                    self.NonStationary1D_ad_re_m()
        #: Full nonstationary case with adv. and reac. for m and n
        else:
            self.NonStationary1D_ad_re_m_n()



    def solve (self,par_solver):
        """
        Note:
            It is necessary to define the class Solver to define self.solver.

        """
        for bc in self.bcs:
            bc.apply(self.u_h.vector())

        problem = NonlinearVariationalProblem(self.F_form, self.u_h, self.bcs, self.J_form)

        self.solver = SolverClass(par_solver,problem)
        sol = self.u_h


        #Sationary case
        if (self.dict['dt']==0):
            self.solver.solve()
            self.dict['filehandler'].write(sol)

        # Compute solution
        else:
            self.dict['filehandler'].write(sol,0.0)
            t = 0.0

            while (t < self.dict['T']):

                print("t=%f"%t)
                t += self.dict['dt']
                self.mn0_.vector()[:] = sol.vector()
                self.solver.solve()


                self.dict['filehandler'].write(sol,t)


    def AssembleBC(self,BCvec):
        """
        Here I define the Dirichlet Boundary conditions.

        Args:
            BCvec (list(Constant)): Collection of all boundary conditions

        Note:
            We use also the set1BC method to define one BC at time.


        Returns:
            It returns the BC vector ready for the initialization of the problem

        """
        BCS = []

        self.mf=FacetFunction("size_t",self.dict['mesh'],0)

        #: Here I build the bc: 1 for bottom, 2 for right, 3 for up, 4 for left
        #: First value in BCvec is the position, the second 1 for m, 2 for
        #: n, 3 for both, the third value represent the value of the bc
        length = len(BCvec)


        for i in range(0,length):

            if BCvec[i][0]==1:
                AutoSubDomain(lambda x,on_boundary:
                                  abs(x[0]-self.dict['Lx'])<DOLFIN_EPS ).mark(self.mf,1)

                BCS.append(self.Set1BC(BCvec,i,1) )


            if BCvec[i][0]==2:
                AutoSubDomain(lambda x,on_boundary:
                                  abs(x[0]-self.dict['Rx'])<DOLFIN_EPS ).mark(self.mf,2)

                BCS.append(self.Set1BC(BCvec,i,2) )

        return BCS

    def Set1BC(self,BCvec,i,j):
        """Summary line.

        Here I append to the bc vector one boundary condition.

        Args:
            BCvec (list(Constant)): Collection of all boundary conditions
            i (int): integer for the loop of the BCvec
            j (int): integer for the market function

        Returns:
            The Dirichlet BC on the appropriate side with the value set in BCvec
            applied on the space set in BCvec

        """
        #: If I have the problem for only m
        if (self.dict['system']=='m'):
            if BCvec[i][1]==1:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MW'].sub(0), BCvec[i][2], \
                    self.mf, j)
            elif BCvec[i][1]==2:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MW'].sub(1), BCvec[i][2], \
                    self.mf, j)
            elif BCvec[i][1]==3:
                assert size(BCvec[i][2])==2, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MW'], BCvec[i][2], \
                    self.mf, j)
        #: Otherwise I have the problem for m and n
        else:
            if BCvec[i][1]==1:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MNW'].sub(0), BCvec[i][2], \
                    self.mf, j)
            elif BCvec[i][1]==2:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MNW'].sub(1), BCvec[i][2], \
                    self.mf, j)
            elif BCvec[i][1]==3:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MNW'].sub(2), BCvec[i][2], \
                    self.mf, j)
            elif BCvec[i][1]==4:
                assert size(BCvec[i][2])==2, "The boundary value must be a   \
                vector of size 2!"
                return DirichletBC(self.FEspaces['MNW'], BCvec[i][2], \
                    self.mf, j)

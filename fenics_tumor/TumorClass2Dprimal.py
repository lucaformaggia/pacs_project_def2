"""
Unit test for TumorClass2Dprimal
"""
__all__ = ['TumorClass2Dprimal']
from dolfin import *
from SolverClass import *
from numpy import size

class TumorClass2Dprimal():
    def  __init__(self,data,BCvec):
        """
        This Class creates the problem for the 2D case with primal approach.

        Args:
            data (dict): Collection of all physical data
            BCvec (list(Constant)): Collection of all boundary conditions

        Raises:
            AttributeError: The ``Raises`` section is a list of all exceptions
                that are relevant to the interface.
            ValueError: If a parameter is the data dictionary is not a constant,
                it raise a warning.

        """

        #: We set the parameters
        try:
            self.dict = {
            'mu': data['mu'],
            'nu': data['nu'],
            'Pm': data['Pm'],
            'gamma': data['gamma'],
            'L': data['L'],
            'nx': data['nx'],
            'ny': data['ny'],
            'dt': data['dt'],
            'T': data['T'],
            'mesh': data['mesh'],
            'mn_init' : data['mn_init'],
            'filehandler': data['filehandler']
            }


        except ValueError:
            if not isinstance(mu,dolfin.Constant):
                warning.warn("mu should be Constant!!")
            if not isinstance(nu,dolfin.Constant):
                warning.warn ("nu should be Constant!!")
            if not isinstance(Pm,dolfin.Constant):
                warning.warn ("Pm should be Constant!!")
            if not isinstance(gamma,dolfin.Constant):
                warning.warn ("gamma should be Constant!!")
            if not isinstance(L,dolfin.Constant):
                warning.warn ("L should be Constant!!")
            if not isinstance(nx,dolfin.Constant):
                warning.warn ("nx should be Constant!!")
            if not isinstance(ny,dolfin.Constant):
                warning.warn ("ny should be Constant!!")
            if not isinstance(dt,dolfin.Constant):
                warning.warn ("dt should be Constant!!")
            if not isinstance(T,dolfin.Constant):
                warning.warn ("T should be Constant!!")

        #: Declaration of all variables
        self.bcs = []
        self.F_form = None
        self.J_form = None
        self.mf = None
        self.FEspaces = None
        self.u_h = None
        self.mn0_ = None

        #: Construction of the problem

        self.AssembleProblem()
        self.bcs = self.AssembleBC(BCvec)

        #: Set of the solver


    def spaceFem(self):
        """

        Here I define the Functional Spaces for the variational problem

        Return:
            It returns the FE space.

        """
        M = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
        N = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
        MNmix = MixedElement([M,N])
        M_fe = FunctionSpace(self.dict['mesh'], M)
        N_fe = FunctionSpace(self.dict['mesh'], N)
        MN = FunctionSpace(self.dict['mesh'], MNmix)
        self.FEspaces = {
        'M': M_fe,
        'N': N_fe,
        'MN': MN,
        }

    def AssembleProblem(self):
        """
        Assemble the problem with the variational form.

        Note:
            The variational form may vary from one file to other, since the kind
            of problem to solve may change due to the test.


        """

        # K_gamma
        K = lambda gam: (1./gam +1.)

        # Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	# Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

      	# G expr
      	G =lambda m,n,Pm,gamma: 200./pi*atan(4.*pos(Pm-power(m,n,gamma)))

        # I construct the Functional Spaces
        self.spaceFem()

        # Now I define the variables for the hole problem
        MN = self.FEspaces['MN']

      	mn_ = Function(MN)

      	(m_,n_) = split(mn_)

      	# Test function
      	dm_ = TestFunction(MN)
      	(dm,dn) = split(dm_)

      	du = TrialFunction(MN)

      	# incremental solution
      	ddu = TrialFunction(MN)

      	# Initial Condition
      	self.mn0_= interpolate(self.dict['mn_init'], MN)

      	(m0, n0) = split(self.mn0_)


        dt = self.dict['dt']
        mu = self.dict['mu']
        nu= self.dict['nu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']

        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})

        # First equation for m
        F0 =  dt*mu*m_*inner(K(gamma)*grad((m_+n_)**gamma), grad(dm))*dxCustom \
            - dt*G(m_,n_,Pm,gamma)*m_*dm*dxCustom \
        +( m_-m0)*dm*dxCustom

        # Second equation for n
        F1 =  dt*nu*n_*inner(K(gamma)*grad((m_+n_)**gamma), grad(dn))*dxCustom \
        +(n_-n0)*dn*dxCustom

        # Sum of the two equations
        F=F0+F1

        # Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], MN)

        F_h = replace(F, {mn_: self.u_h})

        self.F_form = F_h

        J=derivative(F,mn_,ddu)
        J_h = replace(J, {mn_: self.u_h})

        self.J_form = J_h


    def solve (self,par_solver):
        """
        Solve the problem using the solver.

        Note:
            It is necessary to define the class Solver to define self.solver.


        """
        for bc in self.bcs:
            bc.apply(self.u_h.vector())

        problem = NonlinearVariationalProblem(self.F_form, self.u_h, self.bcs, self.J_form)

        self.solver = SolverClass(par_solver,problem)
        sol = self.u_h

        self.dict['filehandler'].write(sol,0.0)

        # Compute solution

        t = 0.0

        while (t < self.dict['T']):
            print("t=%f"%t)
            t += self.dict['dt']
            self.mn0_.vector()[:] = sol.vector()
            self.solver.solve()
            self.dict['filehandler'].write(sol,t)


    def AssembleBC(self,BCvec):
        """

        Here I define the Dirichlet Boundary conditions.

        Args:
            BCvec (list(Constant)): Collection of all boundary conditions

        Note:
            We use also the set1BC method to define one BC at time.


        Returns:
            It returns the BC vector ready for the initialization of the problem

        """
        BCS = []

        self.mf=FacetFunction("size_t",self.dict['mesh'],0)

        # Here I build the bc: 1 for bottom, 2 for right, 3 for up, 4 for left
        # First value in BCvec is the position, the second 1 for m, 2 for
        # n, 3 for both, the third value represent the value of the bc
        length = len(BCvec)

        for i in range(0,length):
            if BCvec[i][0]==1:
                AutoSubDomain(lambda x,on_boundary:
                                     abs(x[1]-0)<DOLFIN_EPS ).mark(self.mf,1)
                BCS.append(self.Set1BC(BCvec,i,1) )


            if BCvec[i][0]==2:
                AutoSubDomain(lambda x,on_boundary:
                                  abs(x[0]-self.dict['L'])<DOLFIN_EPS ).mark(self.mf,2)

                BCS.append(self.Set1BC(BCvec,i,2) )

            if BCvec[i][0]==3:
                AutoSubDomain(lambda x,on_boundary:
                                     abs(x[1]-self.dict['L'])<DOLFIN_EPS ).mark(self.mf,3)

                BCS.append(self.Set1BC(BCvec,i,3) )

            if BCvec[i][0]==4:
                AutoSubDomain(lambda x,on_boundary:
                                  abs(x[0]-0)<DOLFIN_EPS ).mark(self.mf,4)

                BCS.append(self.Set1BC(BCvec,i,4) )

        return BCS

    def Set1BC(self,BCvec,i,j):
        """Summary line.

        Here I append to the bc vector one boundary condition.

        Args:
            BCvec (list(Constant)): Collection of all boundary conditions
            i (int): integer for the loop of the BCvec
            j (int): integer for the market function

        Returns:
            The Dirichlet BC on the appropriate side with the value set in BCvec
            applied on the space set in BCvec

        """

        if BCvec[i][1]==1:
            assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                choice of BC type"
            return DirichletBC(self.FEspaces['MN'].sub(0), BCvec[i][2], self.mf, j)
        elif BCvec[i][1]==2:
            assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                choice of BC type"
            return DirichletBC(self.FEspaces['MN'].sub(1), BCvec[i][2], self.mf, j)
        elif BCvec[i][1]==3:
            assert size(BCvec[i][2])==2, "Mismatch dimension BC value and \
                choice of BC type"
            return DirichletBC(self.FEspaces['MN'], BCvec[i][2], self.mf, j)

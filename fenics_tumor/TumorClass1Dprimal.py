"""
Unit test for TumorClass
"""
__all__ = ['TumorClass1Dprimal']
from dolfin import *
from SolverClass import *
from numpy import size

class TumorClass1Dprimal():
    def  __init__(self,data,BCvec):
        """
        This Class creates the problem for the 1D case with primal approach.

        Args:
            data (dict): Collection of all physical data
            BCvec (list(Constant)): Collection of all boundary conditions

        Raises:
            AttributeError: The ``Raises`` section is a list of all
                exceptions that are relevant to the interface.
            ValueError: If a parameter is the data dictionary is not a
                constant,it raise a warning.

        """

        #: We set the parameters
        try:
            self.dict = {
            'mu': data['mu'],
            'nu': data['nu'],
            'Pm': data['Pm'],
            'gamma': data['gamma'],
            'Lx': data['Lx'],
            'Rx': data['Rx'],
            'nx': data['nx'],
            'dt': data['dt'],
            'T': data['T'],
            'mesh': data['mesh'],
            'mn_init' : data['mn_init'],
            'filehandler': data['filehandler'],
            'system': data['system']
            }


        except ValueError:
            if not isinstance(mu,dolfin.Constant):
                warning.warn("mu should be Constant!!")
            if not isinstance(nu,dolfin.Constant):
                warning.warn ("nu should be Constant!!")
            if not isinstance(Pm,dolfin.Constant):
                warning.warn ("Pm should be Constant!!")
            if not isinstance(gamma,dolfin.Constant):
                warning.warn ("gamma should be Constant!!")
            if not isinstance(Lx,dolfin.Constant):
                warning.warn ("Lx should be Constant!!")
            if not isinstance(nx,dolfin.Constant):
                warning.warn ("nx should be Constant!!")
            if not isinstance(Rx,dolfin.Constant):
                warning.warn ("Rx should be Constant!!")
            if not isinstance(dt,dolfin.Constant):
                warning.warn ("dt should be Constant!!")
            if not isinstance(T,dolfin.Constant):
                warning.warn ("T should be Constant!!")

        #: Declaration of all variables
        self.bcs = []
        self.F_form = None
        self.J_form = None
        self.mf = None
        self.FEspaces = None
        self.u_h = None
        self.mn0_ = None


        #: Construction of the problem

        self.AssembleProblem()
        self.bcs = self.AssembleBC(BCvec)



    def spaceFem(self):
        """
        Here I define the Functional Spaces for the variational problem

        Return:
            It returns the FE space.

        """
        #: If I have the problem only for m:
        if (self.dict['system'] =='m'):
            M = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
            M_fe = FunctionSpace(self.dict['mesh'], M)
            self.FEspaces = {
            'M': M_fe
            }
        #: Otherwise I have the problem for m and n:
        else:
            M = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
            N = FiniteElement("Lagrange",self.dict['mesh'].ufl_cell(), 1)
            MNmix = MixedElement([M,N])
            M_fe = FunctionSpace(self.dict['mesh'], M)
            N_fe = FunctionSpace(self.dict['mesh'], N)
            MN = FunctionSpace(self.dict['mesh'], MNmix)
            self.FEspaces = {
            'M': M_fe,
            'N': N_fe,
            'MN': MN,
            }

    def NonStationary1D_ad_re_m_n(self):
        """
        Assemble the full nonstationary variational form for m and n.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

      	#: G expr
      	G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))

        self.spaceFem()

        #: Now I define the variables for the hole problem
        MN = self.FEspaces['MN']
        #Solution
      	mn_ = Function(MN)

      	(m_,n_) = split(mn_)

        self.mn0_= Function(MN)


        (m0, n0) = split(self.mn0_)


        # Test function
        dm_ = TestFunction(MN)
        (dm,dn) = split(dm_)

        # incremental solution
        ddu = TrialFunction(MN)


        dt = self.dict['dt']
        mu = self.dict['mu']
        nu= self.dict['nu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})


        F0 =  dt*mu*m_*inner(K(gamma)*grad((m_+n_)**gamma), grad(dm))*dxCustom \
            - dt*G(m_,n_,Pm,gamma)*m_*dm*dxCustom\
            +( m_-m0)*dm*dxCustom


        F1 =  dt*nu*n_*inner(K(gamma)*grad((m_+n_)**gamma), grad(dn))*dxCustom \
            +(n_-n0)*dn*dxCustom

        F = F0 + F1

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], MN)

        F_h = replace(F, {mn_: self.u_h})


        self.F_form = F_h


        J=derivative(F,mn_,ddu)
        J_h = replace(J, {mn_: self.u_h})

        self.J_form = J_h

    def NonStationary1D_ad_re_m(self):
        """
        Assemble the nonstationary variational form with adv. and reac.
        for m.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        M = self.FEspaces['M']

        #Solution
        m_ = Function(M)
        self.mn0_ = Function(M)

        # Test function
        dm = TestFunction(M)

        du = TrialFunction(M)


        # incremental solution
        ddu = TrialFunction(M)





      	#: G expr
      	G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))


        dt = self.dict['dt']
        mu = self.dict['mu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})


        F =  dt*mu*m_*inner(K(gamma)*grad((m_)**gamma), grad(dm))*dx \
            - dt*G(m_,0.,Pm,gamma)*m_*dm*dx\
            +( m_-self.mn0_)*dm*dx

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], M)

        F_h = replace(F, {m_: self.u_h})


        self.F_form = F_h


        J=derivative(F,m_,ddu)
        J_h = replace(J, {m_: self.u_h})

        self.J_form = J_h

    def NonStationary1D_ad_m(self):
        """
        Assemble the nonstationary variational form with advection for m.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
        power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

        #: Positive part
        pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        M = self.FEspaces['M']

        #Solution
        m_ = Function(M)
        self.mn0_ = Function(M)

        # Test function
        dm = TestFunction(M)


        # incremental solution
        ddu = TrialFunction(M)




        #: G expr
        G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))


        dt = self.dict['dt']
        mu = self.dict['mu']
        gamma = self.dict['gamma']

        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})


        F =  dt*mu*m_*inner(K(gamma)*grad((m_)**gamma), grad(dm))*dxCustom \
            +( m_-self.mn0_)*dm*dxCustom

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], M)


        F_h = replace(F, {m_: self.u_h})


        self.F_form = F_h


        J=derivative(F,m_,ddu)
        J_h = replace(J, {m_: self.u_h})

        self.J_form = J_h

    def Stationary1D_ad_re_m(self):
        """
        Assemble the stationary variational form with adv. and reac. for m.


        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
      	power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

      	#: Positive part
      	pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        M = self.FEspaces['M']

        #Set the initial guess of nonlinear solver
        self.u_h = interpolate(self.dict['mn_init'], M)


        # Test function
        dm = TestFunction(M)

        # incremental solution
        ddm = TrialFunction(M)


      	#: G expr
      	G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))



        mu = self.dict['mu']
        gamma = self.dict['gamma']
        Pm = self.dict['Pm']
        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})


        F = mu*inner(self.u_h*grad(power(self.u_h,0.,gamma)), grad(dm))*dxCustom\
            -G(self.u_h,0.,Pm,gamma)*self.u_h*dm*dxCustom





        self.F_form = F


        J=derivative(F,self.u_h,ddm)

        self.J_form = J

    def Stationary1D_ad_m(self):
        """
        Assemble the stationary variational form  with advection for m.

        """

        #: K_gamma
        K = lambda gam: (1./gam +1.)

        #: Pressure
        power = lambda m,n,gamma:K(gamma)*(m+n)**gamma

        #: Positive part
        pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

        self.spaceFem()

        #: Now I define the variables for the hole problem
        M = self.FEspaces['M']

        m = Function(M)


        self.u_h = interpolate(self.dict['mn_init'], M)

        # Test function
        dm = TestFunction(M)

        # incremental solution
        ddm = TrialFunction(M)



        #: G expr
        G =lambda m,n,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,n,gamma)))


        dt = self.dict['dt']
        mu = self.dict['mu']
        gamma = self.dict['gamma']

        q_degree = 3

        dxCustom = Measure("dx",domain=self.dict['mesh'],\
            metadata={'quadrature_degree': q_degree})


        F = mu*inner(self.u_h*grad(power(self.u_h,0,gamma)), grad(dm))*dxCustom


        self.F_form = F


        J=derivative(F,self.u_h,ddm)

        self.J_form = J


    def AssembleProblem(self):
        """
        Assemble the problem with the variational form.

        Note:
            The variational form may vary from one file to other, since the kind
            of problem to solve may change due to the test.

        """
        #: Case for only m
        if (self.dict['system']=='m'):
            #: Stationary case
            if (self.dict['dt']==0):
                #: Only advection
                if (self.dict['Pm']==0):
                    self.Stationary1D_ad_m()
                #: With advection and reaction
                else:
                    self.Stationary1D_ad_re_m()
            #: Non-stationary only for m:
            else:
                #: Only advection
                if (self.dict['Pm']==0):
                    self.NonStationary1D_ad_m()
                else:
                    #: With advection and reaction
                    self.NonStationary1D_ad_re_m()
        #: Full nonstationary case with adv. and reac. for m and n
        else:
            self.NonStationary1D_ad_re_m_n()



    def solve (self,par_solver):
        """
        Solve the problem using the solver.

        Note:
            It is necessary to define the class Solver to define self.solver.

        """
        for bc in self.bcs:
            bc.apply(self.u_h.vector())

        problem = NonlinearVariationalProblem(self.F_form, self.u_h, self.bcs, self.J_form)

        self.solver = SolverClass(par_solver,problem)
        sol = self.u_h

        #:Sationary case
        if (self.dict['dt']==0):
            self.solver.solve()
            self.dict['filehandler'].write(sol)

        #: Nonstatioary case
        else:
            self.dict['filehandler'].write(sol,0.0)
            t = 0.0

            while (t < self.dict['T']):

                print("t=%f"%t)
                t += self.dict['dt']
                self.mn0_.vector()[:] = sol.vector()
                self.solver.solve()
                self.dict['filehandler'].write(sol,t)


    def AssembleBC(self,BCvec):
        """
        Here I define the Dirichlet Boundary conditions.

        Args:
            BCvec (list(Constant)): Collection of all boundary conditions

        Note:
            We use also the set1BC method to define one BC at time.


        Returns:
            It returns the BC vector ready for the initialization of the problem

        """
        BCS = []

        self.mf=FacetFunction("size_t",self.dict['mesh'],0)

        #: Here I build the bc: 1 for bottom, 2 for right, 3 for up, 4 for left
        #: First value in BCvec is the position, the second 1 for m, 2 for
        #: n, 3 for both, the third value represent the value of the bc
        length = len(BCvec)


        for i in range(0,length):

            if BCvec[i][0]==1:
                AutoSubDomain(lambda x,on_boundary:
                                  abs(x[0]-self.dict['Lx'])<DOLFIN_EPS ).mark(self.mf,1)

                BCS.append(self.Set1BC(BCvec,i,1) )


            if BCvec[i][0]==2:
                AutoSubDomain(lambda x,on_boundary:
                                  abs(x[0]-self.dict['Rx'])<DOLFIN_EPS ).mark(self.mf,2)

                BCS.append(self.Set1BC(BCvec,i,2) )

        return BCS

    def Set1BC(self,BCvec,i,j):
        """
        Here I append to the bc vector one boundary condition.

        Args:
            BCvec (list(Constant)): Collection of all boundary conditions
            i (int): integer for the loop of the BCvec
            j (int): integer for the market function

        Returns:
            The Dirichlet BC on the appropriate side with the value set in BCvec
            applied on the space set in BCvec

        """
        #: If I have the problem for only m
        if (self.dict['system']=='m'):
            return DirichletBC(self.FEspaces['M'], BCvec[i][1], self.mf, j)
        #: Otherwise I have the problem for m and n
        else:
            if BCvec[i][1]==1:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MN'].sub(0), BCvec[i][2], self.mf, j)
            elif BCvec[i][1]==1:
                assert size(BCvec[i][2])==1, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MN'].sub(1), BCvec[i][2], self.mf, j)
            elif BCvec[i][1]==3:
                assert size(BCvec[i][2])==2, "Mismatch dimension BC value and \
                    choice of BC type"
                return DirichletBC(self.FEspaces['MN'], BCvec[i][2], self.mf, j)

# README #

### What are these steps? ###
This folder contains the steps to compute the solution of the problem
in 1D with the code.

* Omega =Intervalmesh(Lx,Rx)

* B.C: may differ from one case to others, depending on the problem and on the
			method


### Steps:  ###


* b1 primal stationary with reaction  only eq for m
* b2 mixed stationary with reaction  only eq for m


* c1 primal non-stationary with reaction  only eq for m
* c2 mixed non-stationary with reaction  only eq for m

* g1 primal non-stationary with reaction mu=1, nu=2
* g2 mixed non-stationary with reaction  mu=1, nu=2

### How to run the code? ###
To tun the file, simply from the command line
python namefile.py
	where namefile is the name of the file you want to run

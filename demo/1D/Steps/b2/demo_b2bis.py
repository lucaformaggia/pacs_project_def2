from dolfin import *
import numpy as np
import pdb
import os
import sys

parameters["linear_algebra_backend"] = "PETSc"


# Model parameters
mu     = 1.        # viscosity of dividing cells
nu     = 2.        # viscosity of non-dividing cells
gamma  = 4.   # parameter the controls the stiffness of the pressure law
Pm     = 30.       # parameter for G


# Create mesh and define function space
L = 5.
Lx = -6.
Rx = 10.
nx = 500
mesh = IntervalMesh(nx, Lx, Rx)


M = FiniteElement("Lagrange",mesh.ufl_cell(), 1)
W = FiniteElement("Lagrange", mesh.ufl_cell(), 2)
MW = MixedElement([M,W])
MN = FunctionSpace(mesh, MW)
V = FunctionSpace(mesh, W)

#Solution
m = Function(MN)
(m_,w_) = split(m)

# Test function
dm_ = TestFunction(MN)
(dm,dw) = split(dm_)

# incremental solution
ddm = TrialFunction(MN)


mn_init = Expression(('pow( (Pm/(1./gam +1.)) ,(1./gam)) ','0.0'), Pm=Pm,gam=gamma,degree=1)
#mn_init = Expression(('0. ','0.0'), Pm=Pm,gam=gamma,degree=1)

u_h = interpolate(mn_init, MN)


#m.vector()[:] = u_h.vector()



#Power
power = lambda m,gamma:(1.+1./gamma)*(m)**gamma
# Positive part
pos = lambda x: conditional(ge(x,0.0),1.0,0.0)*x

# G expr
G =lambda m,Pm,gamma: 20./pi*atan(4.*pos(Pm-power(m,gamma)))

K=lambda gam: (1./gam +1.)

mf=FacetFunction("size_t",mesh,0)

AutoSubDomain(lambda x,on_boundary:
                         abs(x[0]-Lx)<DOLFIN_EPS ).mark(mf,1)

AutoSubDomain(lambda x,on_boundary:
                         abs(x[0]-Rx)<DOLFIN_EPS ).mark(mf,2)

dsm=Measure("ds",domain=mesh,subdomain_data=mf)

# WHY there was POS?

F0 = -mu*(inner(grad(w_)[0],m_)+ inner(grad(m_)[0],w_))*dm*dx-G(m_,Pm,gamma)*m_*dm*dx

F1 = w_* dw*dx + K(gamma)*inner(grad(dw)[0],m_**gamma)*dx

F = F0 + F1


#- Constant(30.)*dm*dsm(2)
J=derivative(F,m,ddm)

namefolder=os.getcwd()+"/Results"
if os.path.exists(namefolder):
    os.stat(namefolder)
else:
    os.mkdir(namefolder)

filepath=namefolder+"/solutions"
if os.path.exists(filepath+".xdmf"):
    os.remove(filepath+".xdmf")
    os.remove(filepath+".h5")



"""
# HERE I CHECK WHAT THE BCS really DO!!!!!!
# Now they are correct.
# Define boundary condition
g = Constant(1.0)
bcs = [DirichletBC(MN, g, mf, 1),
      DirichletBC(MN,g,mf,2)]

m0=Function(MN)
for bc in bcs:
    bc.apply(m0.vector())

print(np.where(m0.vector().array()>0))


#print(BoundaryMesh(mesh,"exterior").coordinates())

for bc in bcs:
    dofvalues = bc.get_boundary_values()
    f=Function(MN)
    print(dofvalues)
    #plot(f)
    #interactive()
#filehandler=XDMFFile(mpi_comm_world(),filepath+"0.xdmf")
#filehandler.write(m0)
"""

#g = Function(V)
g = Constant((0.))

bcs = [DirichletBC(MN.sub(0), g, mf, 2),DirichletBC(MN.sub(0), Constant((Pm/K(gamma))**(1./gamma)), mf, 1),\
    DirichletBC(MN.sub(1), g, mf, 2),DirichletBC(MN.sub(1), Constant((0.)), mf, 1)]

F_h = replace(F, {m: u_h})
J_h = replace(J, {m: u_h})

# Compute solution
problem = NonlinearVariationalProblem(F_h, u_h, bcs, J_h)

# Define the solver parameters
snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver": {"linear_solver": "lu",
                                          "maximum_iterations": 400,
                                          "report": True,
                                          "error_on_nonconvergence": True,
                                          "line_search": 'basic',
                                          "method": 'default'}}

# Set up the non-linear solver
solver  = NonlinearVariationalSolver(problem)
solver.parameters.update(snes_solver_parameters)
solver.solve()


#solve(F==0, m, bc, solver_parameters={"newton_solver":
##                                        {"relative_tolerance": 1e-6}})

filehandler=XDMFFile(mpi_comm_world(),filepath+".xdmf")
filehandler.write(u_h.split()[0])

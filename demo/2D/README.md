# This Folder contains the demo files for the 2D case #

If you want to run the stable case, write on the command line
```
python demo_2D_primal_nonlinear_stable.py
```
If you want to run the unstable case, write on the command line
```
python demo_2D_primal_nonlinear_unstable.py
```
